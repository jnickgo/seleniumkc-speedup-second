package com.automationpractice.jsoup;

import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.util.HashMap;
import java.util.Map;

/**
 * Example of sign-in form with JSoup.
 */
public class SignInTest {

    private static final String URI = "http://automationpractice.com/index.php?controller=authentication";
    private Map cookies;



    public void getData(HashMap<String, String> formFields) throws Exception {

        Connection conn;

        conn = Jsoup.connect(URI)
                .userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0")
                .cookies(cookies)
                .timeout(0)
                .data(formFields)
                .method(Connection.Method.POST);
    }

}
