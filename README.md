# SeleniumKC Speedup Second Demonstration

A demonstration project to show some alternatives to Selenium in verifying website content.

## About this project
Previously I was working on some projects that I needed to scrape and verify some data within some web pages. I needed to do this headlessly and I didn't really want to go to the trouble of installing Chrome and all it's binaries.


## Enter JSoup


## Project Setup

* Gradle
* JUnit 5
* JSoup
* REST-assured

